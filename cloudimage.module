<?php

/**
 * Implements hook_preprocess_image().
 */
function cloudimage_preprocess_image(&$variables)
{
  $variables['attributes']['srcset'] = FALSE;

  $config = \Drupal::config('cloudimage.admin_settings');
  $activation = $config->get('activation');
  $token_or_cname = $config->get('token_or_cname');

  if ($activation && $token_or_cname != '') {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $standard_mode = $config->get('standard_mode');

    if ($standard_mode) {
      $remove_v7 = $config->get('remove_v7');
      $v7 = '';
      if (!$remove_v7) {
        $v7 = 'v7/';
      }

      $image_quality = $config->get('image_quality');
      if ($image_quality != '' && $image_quality <= 100) {
        $quality = '?q=' . $image_quality;
        if (strpos($variables['attributes']['src'], '?')) {
          $quality = '&q=' . $image_quality;
        }
        $variables['attributes']['src'] = $variables['attributes']['src'] . $quality;
      }

      $custom_library = $config->get('custom_library');
      if ($custom_library != '') {
        if (strpos($variables['attributes']['src'], '?')) {
          $variables['attributes']['src'] = $variables['attributes']['src'] . '&' . $custom_library;
        } else {
          $variables['attributes']['src'] = $variables['attributes']['src'] . '?' . $custom_library;
        }
      }

      $prevent_image_upsize = $config->get('prevent_image_upsize');
      if ($prevent_image_upsize) {
        if (strpos($variables['attributes']['src'], '?')) {
          $variables['attributes']['src'] = $variables['attributes']['src'] . '&org_if_sml=1';
        } else {
          $variables['attributes']['src'] = $variables['attributes']['src'] . '?org_if_sml=1';
        }
      }

      $ciUrl = 'https://' . $token_or_cname . '.cloudimg.io/' . $v7;
      if (strpos($token_or_cname, '.')) {
        $ciUrl = 'https://' . $token_or_cname . '/' . $v7;
      }

      if (isset($variables['attributes']['src'])) {
        $variables['attributes']['src'] = $ciUrl . $host . $variables['attributes']['src'];
      }
    } else {
      // Check if the image has a 'src' attribute.
      if (isset($variables['attributes']['src'])) {
        // Change the 'src' attribute to 'ci-src'.
        $variables['attributes']['ci-src'] = $host . $variables['attributes']['src'];
        unset($variables['attributes']['src']);
      }
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function cloudimage_page_attachments(array &$attachments) {
  $config = \Drupal::config('cloudimage.admin_settings');
  $activation = $config->get('activation');
  $token_or_cname = $config->get('token_or_cname');
  $standard_mode = $config->get('standard_mode');
  if ($activation && $token_or_cname != '' && !$standard_mode) {
    $attachments['#attached']['library'][] = 'cloudimage/responsive';

    $config_data = [
      'token_or_cname' => $config->get('token_or_cname'),
      'activation' => $config->get('activation'),
      'standard_mode' => $config->get('standard_mode'),
      'use_original_url' => $config->get('use_original_url'),
      'lazy_loading' => $config->get('lazy_loading'),
      'ignore_svg_image' => $config->get('ignore_svg_image'),
      'prevent_image_upsize' => $config->get('prevent_image_upsize'),
      'image_quality' => $config->get('image_quality'),
      'maximum_pixel_ratio' => $config->get('maximum_pixel_ratio'),
      'remove_v7' => $config->get('remove_v7'),
      'image_size_attributes' => $config->get('image_size_attributes'),
      'custom_function' => $config->get('custom_function'),
      'custom_library' => $config->get('custom_library'),
    ];

    $attachments['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#value' => 'const ciSettings = ' . json_encode($config_data) . ';'
      ],
      'ciSettings',
    ];

    $attachments['#attached']['library'][] = 'cloudimage/custom-js';
    $lazy_loading = $config->get('lazy_loading');
    if ($lazy_loading) {
      $attachments['#attached']['library'][] = 'cloudimage/lazy-loading';
    }
  }
}


