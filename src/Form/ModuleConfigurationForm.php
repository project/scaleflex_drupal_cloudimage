<?php

namespace Drupal\cloudimage\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudimage_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudimage.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cloudimage.admin_settings');

    $token_or_cname = $config->get('token_or_cname');
    $activation = $config->get('activation');
    $standard_mode = $config->get('standard_mode');
    $use_original_url = $config->get('use_original_url');
    $lazy_loading = $config->get('lazy_loading');
    $ignore_svg_image = $config->get('ignore_svg_image');
    $prevent_image_upsize = $config->get('prevent_image_upsize');
    $image_quality = $config->get('image_quality');
    $maximum_pixel_ratio = $config->get('maximum_pixel_ratio');
    $remove_v7 = $config->get('remove_v7');
    $image_size_attributes = $config->get('image_size_attributes');
    $custom_function = $config->get('custom_function');
    $custom_library = $config->get('custom_library');

    $form['activation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activation'),
      '#default_value' => $activation,
      '#description' => $this->t('Enable/Disable the Module'),
    ];
    $form['token_or_cname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token or CNAME'),
      '#default_value' => $token_or_cname,
      '#description' => $this->t('Cloudimage token or custom domain'),
    ];
    $form['standard_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Standard Mode'),
      '#default_value' => $standard_mode,
      '#description' => $this->t('Replace image URLs not using any Javascript or Javascript library'),
    ];
    $form['use_original_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Origin URL'),
      '#default_value' => $use_original_url,
      '#description' => $this->t('If enabled, the plugin will only add query parameters to the image source URL, avoiding double CDN in some cases, like if you have aliases configured'),
    ];
    $form['lazy_loading'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lazy Loading'),
      '#default_value' => $lazy_loading,
      '#description' => $this->t('If enabled, only images close to the current viewpoint will be loaded'),
    ];
    $form['ignore_svg_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore SVG Image'),
      '#default_value' => $ignore_svg_image,
      '#description' => $this->t('By default, No'),
    ];
    $form['prevent_image_upsize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent Image Upsize'),
      '#default_value' => $prevent_image_upsize,
      '#description' => $this->t('If you set Maximum "Pixel ratio" equal to 2, but some of your assets does not have min retina size(at least 2560x960), please enable this to prevent image resized. By default, Yes'),
    ];
    $form['image_quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Quality'),
      '#default_value' => $image_quality,
      '#options' => [
        '100' => '100',
        '95' => '95',
        '90' => '90',
        '85' => '85',
        '80' => '80',
        '75' => '75',
        '70' => '70',
        '65' => '65',
        '60' => '60',
        '55' => '55',
        '50' => '50',
        '45' => '45',
        '40' => '40',
        '35' => '35',
        '30' => '30',
        '25' => '25',
        '20' => '20',
        '15' => '15',
        '10' => '10',
        '5' => '5'
      ],
      '#description' => $this->t('The smaller the value, the more your image will compressed. Careful - the quality of the image will decrease as well. By default, 90'),
    ];
    $form['maximum_pixel_ratio'] = [
      '#type' => 'select',
      '#title' => $this->t('Maximum "Pixel Ratio"'),
      '#default_value' => $maximum_pixel_ratio,
      '#options' => [
        '1' => '1',
        '1.5' => '1.5',
        '2' => '2'
      ],
      '#description' => $this->t('List of supported device pixel ratios, eg 2 for Retina devices'),
    ];
    $form['remove_v7'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove V7'),
      '#default_value' => $remove_v7,
      '#description' => $this->t('Removes the "/v7" part in URL format. Activate for token created after October 20th, 2021'),
    ];
    $form['image_size_attributes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image Size Attributes'),
      '#default_value' => $image_size_attributes,
      '#description' => $this->t('Used to calculate width and height of images'),
    ];
    $form['custom_function'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom js function'),
      '#default_value' => $custom_function,
      '#description' => $this->t('The valid js function starting with { and finishing with }'),
    ];
    $form['custom_library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom library options'),
      '#default_value' => $custom_library,
      '#description' => $this->t('Automatically adds Cloudimage parameters for all images, e.g. watermark=1 to put a watermark on all images. The list of all available parameters can be found on docs.cloudimage.io'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('cloudimage.admin_settings')
      ->set('activation', $form_state->getValue('activation'))
      ->set('token_or_cname', $form_state->getValue('token_or_cname'))
      ->set('standard_mode', $form_state->getValue('standard_mode'))
      ->set('use_original_url', $form_state->getValue('use_original_url'))
      ->set('lazy_loading', $form_state->getValue('lazy_loading'))
      ->set('ignore_svg_image', $form_state->getValue('ignore_svg_image'))
      ->set('prevent_image_upsize', $form_state->getValue('prevent_image_upsize'))
      ->set('image_quality', $form_state->getValue('image_quality'))
      ->set('maximum_pixel_ratio', $form_state->getValue('maximum_pixel_ratio'))
      ->set('remove_v7', $form_state->getValue('remove_v7'))
      ->set('image_size_attributes', $form_state->getValue('image_size_attributes'))
      ->set('custom_function', $form_state->getValue('custom_function'))
      ->set('custom_library', $form_state->getValue('custom_library'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
